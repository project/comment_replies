Readme
------
This module notifies users when someone has replied to one of their comments.

Send comments to Gwen Park at: http://drupal.org/user/99925.

Requirements
------------
This module requires Drupal 5.x and the comment module. If the optional comment_redirect module is installed, this module will always generate working links to comments (see http://drupal.org/projects/comment_redirect).

Installation
------------
1. Copy the comment_replies directory and its contents to the Drupal modules directory, probably /sites/all/modules/.

2. Enable the Comment Replies module in the modules admin page /admin/build/modules.

Developers
----------
If you need to test, there's a php file and associated include file that will generate a bunch of test comments.

Todo
----
1. add repopulate table option to settings form
1. upgrade to drupal 6

Credits
-------
Written by Gwen Park, JB Christy and Annette Tisdale. Funded by OurChart.com.
