<?php

/*
 * This function generates a bunch of replies to a user's comments
 */
function comment_replies_generate_comments($uid, $limit = 100) {
  $users = comment_replies_get_users($limit);
  $usercount = count($users);
  $query = "SELECT * FROM {comments} WHERE uid = %d AND status = %d";
  $query .= " LIMIT 0, %d";
  $query_args = array($uid, COMMENT_PUBLISHED, $limit);
  $result = db_query($query, $query_args);
  while ($comment = db_fetch_object($result)) {
    $random = rand(0, $usercount - 1);
    $user = $users[$random];
    $form_values = comment_replies_comment_form($user, $comment);
    $cid = comment_save($form_values);
    if ($cid && is_numeric($cid)) {
      comment_replies_change_comment_timestamp($cid);
    }
  }
}

/*
 * This function populates the comment form to be submitted
 */
function comment_replies_comment_form($user, $comment) {
  static $count;
  $count++;
  $form = array(
    'name' => $user->name,
    'uid' => $user->uid,
    'subject' => t('test subject @count', array('@count' => $count)),
    'comment' => t('test comment @count', array('@count' => $count)),
    'pid' => $comment->cid,
    'nid' => $comment->nid,
  );
  return $form;
}

/*
 * This function gets some users
 */
function comment_replies_get_users($limit) {
  $query = "SELECT * FROM {users} WHERE status = %d";
  $query .= " ORDER BY access DESC";
  $query .= " LIMIT 0, %d";
  $query_args = array(1, $limit);
  $result = db_query($query, $query_args);
  while ($object = db_fetch_object($result)) {
    $users[] = $object;
  }
  return $users;
}

/*
 * There's no way to pass a timestamp into comment_save(), so this function
 * is a workaround to change the timestamp after the fact
 */
function comment_replies_change_comment_timestamp($cid) {
  $days = (int)rand(0, 10);
  $timestamp = mktime(0, 0, 0, date("m")  , date("d") - $days, date("Y"));
  $query = "UPDATE {comments} SET timestamp = %d WHERE cid = %d";
  $query_args = array($timestamp, $cid);
  db_query($query, $query_args);
  comment_replies_change_comment_replies_timestamp($cid, $timestamp);
}

/*
 * This function changes the comment replies timestamp
 */
function comment_replies_change_comment_replies_timestamp($cid, $timestamp) {
  $query = "UPDATE {comment_replies} SET timestamp = %d WHERE cid = %d";
  $query_args = array($timestamp, $cid);
  db_query($query, $query_args);
}
