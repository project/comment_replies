<?php

include_once './comment_replies_dev.inc';
chdir ($_SERVER['DOCUMENT_ROOT']);
include_once 'includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

global $user;
comment_replies_generate_comments($user->uid);
